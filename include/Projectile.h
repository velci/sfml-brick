#ifndef PROJECTILE_H
#define PROJECTILE_H

#include "GameObject.h"
#include <SFML/Graphics.hpp>

class Projectile : public GameObject {
    public:
        Projectile(int x, int y, int width, int height, sf::Vector2f v, sf::Color color);
        virtual ~Projectile();

        /**
            Reflect the projectile by v
            @param v an vector
        */
        void reflect(sf::Vector2f v);
    protected:
    private:
};

#endif // PROJECTILE_H
