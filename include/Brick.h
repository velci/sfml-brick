#ifndef BRICK_H
#define BRICK_H

#include "GameObject.h"
#include "Color.h"

class Brick : public GameObject {
    public:
        Brick(int x, int y, const sf::Color color);
        virtual ~Brick();
        static const int WIDTH = 64;
        static const int HEIGHT = 32;
    protected:
    private:
        bool containsPowerup;
};

#endif // BRICK_H
