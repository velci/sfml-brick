#ifndef BALL_H
#define BALL_H

#include "Projectile.h"
#include "Color.h"

class Ball : public Projectile {
    public:
        Ball(int x, int y);
        virtual ~Ball();
        bool isLive();
        void setLive(bool live);
        static const int WIDTH = 32;
        static const int HEIGHT = 32;
    protected:
    private:
        bool live;

};

#endif // BALL_H
