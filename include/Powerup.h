#ifndef POWERUP_H
#define POWERUP_H

#include "GameObject.h"
#include "Color.h"

class Powerup : public GameObject {
    public:
        enum Type {
            LAZER,
            EXTRA_LIFE
        };
        Powerup(int x, int y);
        virtual ~Powerup();
        int getType();
        void setType(int type);
        static const int WIDTH = 32;
        static const int HEIGHT = 32;
    protected:
    private:
        int type;
};

#endif // POWERUP_H
