#ifndef PLAYER_H
#define PLAYER_H

#include "GameObject.h"
#include "Color.h"

class Player : public GameObject {
    public:
        Player(int x, int y);
        virtual ~Player();
        int getScore();
        void setScore(int score);
        int getLives();
        void setLives(int lives);
        bool hasLazers();
        void setLazers(bool lazers);
        int getFireRate();
        void setFireRate(int fireRate);
        bool canFire();
        static const int WIDTH = 128;
        static const int HEIGHT = 32;
        static const int DEFAULT_FIRE_RATE = 2;
    protected:
    private:
        unsigned int fireRate;
        unsigned int score;
        unsigned int lives;
        bool lazers;
        sf::Clock fireClock;

};

#endif // PLAYER_H
