#ifndef UTIL_H
#define UTIL_H

#include <SFML/Graphics.hpp>
#include <sstream>

class Util
{
    public:
        enum Direction {
            LEFT,
            RIGHT,
            UP,
            DOWN,
            NONE
        };

        /**
            Creates a unit vector in the given direction
            @param d an enum direction
            @return a Vector2f with length of 1
        */
        static sf::Vector2f createVector(Direction d);

        /**
            Creates a unit vector in the given directions
            @param d1 an enum direction
            @param d2 an enum direction
            @return a Vector2f with length of 1
        */
        static sf::Vector2f createVector(Direction d1, Direction d2);

        /**
            Converts a vector into a unit vector.
            @see http://en.wikipedia.org/wiki/Unit_vector
            @param v a Vector2f
            @return A Vector2f with length of 1.
        */
        static sf::Vector2f unit(sf::Vector2f v);

        /**
            Calculates the Euclidean norm of a vector.
            @see http://en.wikipedia.org/wiki/Norm_(mathematics)
            @param v a Vector2f
            @return the Euclidean norm of the vector.
        */
        static float norm(sf::Vector2f v);

        /**
            Calculates the dot product of two vectors
            @see http://en.wikipedia.org/wiki/Dot_product
            @param v1 a Vector2f
            @param v2 a Vector2f
            @return the dot product of two vectors.
        */
        static float dot(sf::Vector2f v1, sf::Vector2f v2);

        /**
            Calculates the sum of two vectors
            @see http://en.wikipedia.org/wiki/Euclidean_vector#Addition_and_subtraction
            @param v1 a Vector2f
            @param v2 a Vector2f
            @return the sum of two vectors.
        */
        static sf::Vector2f add(sf::Vector2f v1, sf::Vector2f v2);

        /**
            Reverses a vector
            @param v1 a Vector2f
            @return a reversed vector.
        */
        static sf::Vector2f mirror(sf::Vector2f v);

        /**
            Reflects v1 by v2
            @see http://www.3dkingdoms.com/weekly/weekly.php?a=2
            @param v1 a Vector2f
            @param v2 a Vector2f
            @return a Vector2f which is the reflection of v1 by v2
        */
        static sf::Vector2f reflect(sf::Vector2f v1, sf::Vector2f v2);

        /**
            Converts an object to a string
            Note: the function body for the function template cannot be in the cpp file.
            @param o an object
            @return a string representation of o
        */
        template<class T>
        inline static std::string toString(T o) {
            std::stringstream ss;
            ss << o;
            return ss.str();
        }

    protected:
    private:
        // since Util has all static methods, we never want to be able to instantiate it
        Util();
};

#endif // UTIL_H
