#ifndef GAME_H
#define GAME_H

#include <SFML/Graphics.hpp>
#include "Brick.h"
#include "Powerup.h"
#include "Player.h"
#include "Ball.h"
#include "Lazer.h"

class Game {
    public:
        Game();
        virtual ~Game();
        bool initialize();
        int run();
    protected:
        void setupGame();
        void setupLevel();
        void setupRound();
        void createTitle();
        void processEvents();
        void checkCollisions();
        void update(float elapsedTime);
        void draw();
    private:
        static const int SCREEN_WIDTH = 800;
        static const int SCREEN_HEIGHT = 600;

        static const int FRAMES_PER_SECOND = 60;
        static const int MAX_FRAMESKIP = 10;

        static const int BUFFER_LEFT = 64;
        static const int BUFFER_RIGHT = 64;
        static const int BUFFER_TOP = 64;
        static const int BUFFER_BOTTOM = SCREEN_HEIGHT / 2;

        sf::RenderWindow window;
        Player player1;
        Ball ball;
        std::vector<Brick> bricks;
        std::vector<Powerup> powerups;
        std::vector<Lazer> lazers;

        sf::String player1LivesText;
        sf::String fpsText;
        sf::String gameOverText;
        sf::String gameWonText;
        sf::String restartText;
        sf::Sprite backgroundSprite;

        // title screen
        std::vector<Brick> titleBricks;
        sf::String titleText;

        enum states {INTRO, PLAYING, GAME_WON, GAME_LOST};
        int gameState;
};

#endif // GAME_H
