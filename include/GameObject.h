#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <SFML/Graphics.hpp>

class GameObject {
    public:
        GameObject(int x, int y, int width, int height, const sf::Color color);
        virtual ~GameObject();
        sf::Sprite * getSprite();
        sf::Vector2f getDirection();
        void setDirection(sf::Vector2f direction);
        float getVelocity();
        void setVelocity(float velocity);
        void setPosition(float x, float y);
        void setPosition(sf::Vector2f position);
        void tick(float elapsedTime);
        void setColor(sf::Color color);
        void draw(sf::RenderWindow &window);
    protected:
        sf::Sprite sprite;
    private:
        sf::Vector2f direction;
        float velocity;
        sf::Image image;
};

#endif // GAMEOBJECT_H
