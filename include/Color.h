#ifndef COLOR_H
#define COLOR_H

#include <SFML/Graphics.hpp>

class Color {
    public:
        virtual ~Color();
        static const sf::Color WHITE;
        static const sf::Color BLACK;

        static const sf::Color RED;
        static const sf::Color GREEN;
        static const sf::Color BLUE;
        static const sf::Color PURPLE;
        static const sf::Color YELLOW;
        static const sf::Color CYAN;
        static const sf::Color ORANGE;
        static sf::Color getColor(int n);
    protected:
    private:
        Color();
};

#endif // COLOR_H
