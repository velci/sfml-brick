#ifndef LAZER_H
#define LAZER_H

#include "Projectile.h"

class Lazer : public Projectile {
    public:
        Lazer(int x, int y);
        virtual ~Lazer();
        static const int WIDTH = 5;
        static const int HEIGHT = 100;
    protected:
    private:
};

#endif // LAZER_H
