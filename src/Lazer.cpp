#include "../include/Lazer.h"
#include "../include/Color.h"
#include "../include/Util.h"

Lazer::Lazer(int x, int y) : Projectile(x, y, WIDTH, HEIGHT, Util::createVector(Util::UP), Color::RED) {

}

Lazer::~Lazer() {
    //dtor
}
