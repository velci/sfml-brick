#include "../include/GameObject.h"
#include "../include/Color.h"
GameObject::GameObject(int x, int y, int width, int height, const sf::Color color) : sprite(sf::Image(width, height, color)) {
    sf::Vector2f temp(0,0);
    setDirection(temp);

    setPosition(x, y);

    // fix colorization issue
    setColor(color);
}

GameObject::~GameObject() {
}

sf::Sprite * GameObject::getSprite() {
    return &sprite;
}

float GameObject::getVelocity() {
    return velocity;
}

void GameObject::setVelocity(float velocity) {
    this->velocity = velocity;
}

sf::Vector2f GameObject::getDirection() {
    return direction;
}

void GameObject::setDirection(sf::Vector2f direction) {
    this->direction.x = direction.x;
    this->direction.y = direction.y;
}

void GameObject::setPosition(float x, float y) {
    getSprite()->SetX(x);
    getSprite()->SetY(y);
}

void GameObject::setPosition(sf::Vector2f position) {
    getSprite()->SetX(position.x);
    getSprite()->SetY(position.y);
}

void GameObject::setColor(sf::Color color) {
    getSprite()->SetColor(color);
}

void GameObject::tick(float elapsedTime) {
    getSprite()->Move(direction.x * velocity * elapsedTime , direction.y * velocity * elapsedTime);
}

void GameObject::draw(sf::RenderWindow &window) {
    window.Draw(sprite);
}
