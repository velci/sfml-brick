#include "../include/Util.h"

Util::Util() {
    //ctor
}

sf::Vector2f Util::createVector(Direction d) {
    return createVector(d, NONE);
}

sf::Vector2f Util::createVector(Direction d1, Direction d2) {

    sf::Vector2f v(0,0);

    switch(d1) {
    case LEFT:
        v.x = -1;
        break;
    case RIGHT:
        v.x = 1;
        break;
    case UP:
        v.y = -1;
        break;
    case DOWN:
        v.y = 1;
        break;
    default:
        break;
    }

    switch(d2) {
    case LEFT:
        v.x = -1;
        break;
    case RIGHT:
        v.x = 1;
        break;
    case UP:
        v.y = -1;
        break;
    case DOWN:
        v.y = 1;
        break;
    default:
        break;
    }

    return Util::unit(v);
}

sf::Vector2f Util::unit(sf::Vector2f v) {
    float norm = Util::norm(v);
    if (norm > 0) {
        sf::Vector2f u(v.x/norm, v.y/norm);
        return u;
    } else {
        return v;
    }
}

float Util::norm(sf::Vector2f v) {
    return sqrt(v.x * v.x + v.y * v.y);
}

float Util::dot(sf::Vector2f v1, sf::Vector2f v2) {
    return v1.x * v2.x + v1.y * v2.y;
}

sf::Vector2f Util::add(sf::Vector2f v1, sf::Vector2f v2) {
    return sf::Vector2f(v1.x + v2.x, v1.y + v2.y);
}

sf::Vector2f Util::mirror(sf::Vector2f v) {
    return sf::Vector2f(v.x * -1, v.y * -1);
}

sf::Vector2f Util::reflect(sf::Vector2f v, sf::Vector2f n) {
    float d = dot(v, n);
    sf::Vector2f reflection(-2 * d * n.x + v.x, -2 * d * n.y + v.y);
    return reflection;
}
