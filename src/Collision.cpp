
/*
 * File:   collision.cpp
 * Author: Nick
 *
 * Created on 30 January 2009, 11:02
 */
#include <SFML/Graphics.hpp>
#include "../include/Collision.h"
#include "../include/Util.h"

Collision::Collision() {
}

Collision::~Collision() {
}

sf::IntRect Collision::GetAABB(const sf::Sprite& Object) {

    //Get the top left corner of the sprite regardless of the sprite's center
    //This is in Global Coordinates so we can put the rectangle back into the right place
    sf::Vector2f pos = Object.TransformToGlobal(sf::Vector2f(0, 0));

    //Store the size so we can calculate the other corners
    sf::Vector2f size = Object.GetSize();

    float Angle = Object.GetRotation();

    //Bail out early if the sprite isn't rotated
    if (Angle == 0.0f) {
        return sf::IntRect(static_cast<int> (pos.x),
                static_cast<int> (pos.y),
                static_cast<int> (pos.x + size.x),
                static_cast<int> (pos.y + size.y));
    }

    //Calculate the other points as vectors from (0,0)
    //Imagine sf::Vector2f A(0,0); but its not necessary
    //as rotation is around this point.
    sf::Vector2f B(size.x, 0);
    sf::Vector2f C(size.x, size.y);
    sf::Vector2f D(0, size.y);

    //Rotate the points to match the sprite rotation
    B = RotatePoint(B, Angle);
    C = RotatePoint(C, Angle);
    D = RotatePoint(D, Angle);

    //Round off to int and set the four corners of our Rect
    int Left = static_cast<int> (MinValue(0.0f, B.x, C.x, D.x));
    int Top = static_cast<int> (MinValue(0.0f, B.y, C.y, D.y));
    int Right = static_cast<int> (MaxValue(0.0f, B.x, C.x, D.x));
    int Bottom = static_cast<int> (MaxValue(0.0f, B.y, C.y, D.y));

    //Create a Rect from out points and move it back to the correct position on the screen
    sf::IntRect AABB = sf::IntRect(Left, Top, Right, Bottom);
    AABB.Offset(static_cast<int> (pos.x), static_cast<int> (pos.y));
    return AABB;
}

float Collision::MinValue(float a, float b, float c, float d) {
    float min = a;

    min = (b < min ? b : min);
    min = (c < min ? c : min);
    min = (d < min ? d : min);

    return min;
}

float Collision::MaxValue(float a, float b, float c, float d) {
    float max = a;

    max = (b > max ? b : max);
    max = (c > max ? c : max);
    max = (d > max ? d : max);

    return max;
}

sf::Vector2f Collision::RotatePoint(const sf::Vector2f& Point, float Angle) {
    Angle = Angle * RADIANS_PER_DEGREE;
    sf::Vector2f RotatedPoint;
    RotatedPoint.x = Point.x * cos(Angle) + Point.y * sin(Angle);
    RotatedPoint.y = -Point.x * sin(Angle) + Point.y * cos(Angle);
    return RotatedPoint;
}

bool Collision::SimpleBoundingBoxTest(const sf::Sprite& Object1, const sf::Sprite& Object2) {

    sf::IntRect temp1 = GetAABB(Object1);
    sf::IntRect temp2 = GetAABB(Object2);

    // check whether the corners of Object 2 are within the bounds of Object 1
    return temp1.Intersects(temp2);
}

sf::Vector2f Collision::collide(const sf::Sprite& s1, const sf::Sprite& s2) {
    sf::Vector2f results;

    sf::IntRect t1 = GetAABB(s1);
    sf::IntRect t2 = GetAABB(s2);

    sf::IntRect overlap;
    if (t1.Intersects(t2, &overlap)) {

        sf::Vector2f horizontal;
        if (overlap.Left == t2.Left) {
            horizontal = Util::createVector(Util::RIGHT);
        } else if (overlap.Right == t2.Right) {
            horizontal = Util::createVector(Util::LEFT);
        }

        sf::Vector2f vertical;
        if (overlap.Top == t2.Top) {
            vertical = Util::createVector(Util::DOWN);
        } if (overlap.Bottom == t2.Bottom) {
            vertical = Util::createVector(Util::UP);
        }

        results = Util::unit(Util::add(horizontal, vertical));
    }

    return results;
}
