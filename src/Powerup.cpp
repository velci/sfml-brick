#include "../include/Powerup.h"

Powerup::Powerup(int x, int y) : GameObject(x, y, WIDTH, HEIGHT, Color::CYAN) {
    sf::Vector2f temp(0,1);
    setDirection(temp);

    int type = rand() % 2;
    this->type = type;

    setColor(Color::getColor(type));
}

Powerup::~Powerup() {
    //dtor
}

int Powerup::getType() {
    return type;
}

void Powerup::setType(int type) {
    this->type = type;
}
