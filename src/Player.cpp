#include "../include/Player.h"
#include <SFML/Graphics.hpp>

Player::Player(int x, int y) : GameObject(x, y, WIDTH, HEIGHT, Color::WHITE) {
    score = 0;
    lives = 3;

    fireClock.Reset();
    fireRate = DEFAULT_FIRE_RATE;
}

Player::~Player() {
    //dtor
}

int Player::getScore() {
    return score;
}

void Player::setScore(int score) {
    this->score = score;
}

int Player::getLives() {
    return lives;
}

void Player::setLives(int lives) {
    this->lives = lives;
}

bool Player::hasLazers() {
    return lazers;
}

void Player::setLazers(bool lazers) {
    this->lazers = lazers;
}

int Player::getFireRate() {
    return fireRate;
}

void Player::setFireRate(int fireRate) {
    this->fireRate = fireRate;
}

bool Player::canFire() {
    bool canFire = false;
    if (fireClock.GetElapsedTime() > 1.0/fireRate) {
        canFire = true;
        fireClock.Reset();
    }
    return canFire;
}
