#include "../include/Color.h"
#include <SFML/Graphics.hpp>

Color::Color() {
    //ctor
}

Color::~Color() {
    //dtor
}

const sf::Color Color::WHITE = sf::Color(255, 255, 255);
const sf::Color Color::BLACK = sf::Color(0, 0, 0);
const sf::Color Color::RED = sf::Color(255, 0, 0);
const sf::Color Color::GREEN = sf::Color(0, 255, 0);
const sf::Color Color::BLUE = sf::Color(0, 0, 255);
const sf::Color Color::PURPLE = sf::Color(255, 0, 255);
const sf::Color Color::YELLOW = sf::Color(255, 255, 0);
const sf::Color Color::CYAN = sf::Color(0, 255, 255);

const sf::Color Color::ORANGE = sf::Color(255, 160, 0);

sf::Color Color::getColor(int n) {

    sf::Color color;

    switch (n) {
    case 0:
        color = RED;
        break;
    case 1:
        color = GREEN;
        break;
    case 2:
        color = BLUE;
        break;
    case 3:
        color = PURPLE;
        break;
    case 4:
        color = YELLOW;
        break;
    case 5:
        color = CYAN;
        break;
    case 6:
        color = ORANGE;
        break;
    default:
        color = WHITE;
        break;
    }

    return color;
}
