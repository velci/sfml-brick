#include "../include/Ball.h"
#include "../include/Util.h"
#include <SFML/Graphics.hpp>

Ball::Ball(int x, int y) : Projectile(x, y, WIDTH, HEIGHT, Util::createVector(Util::NONE), Color::WHITE) {
    live = false;
}

Ball::~Ball() {
    //dtor
}

bool Ball::isLive() {
    return live;
}

void Ball::setLive(bool live) {
    this->live = live;
}
