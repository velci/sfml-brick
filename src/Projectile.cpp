#include "../include/Projectile.h"
#include "../include/Color.h"
#include "../include/Util.h"

Projectile::Projectile(int x, int y, int width, int height, sf::Vector2f v, sf::Color color) : GameObject(x, y, width, height, color) {
    sf::Vector2f temp(v.x, v.y);
    setDirection(temp);
}

Projectile::~Projectile() {
    //dtor
}

void Projectile::reflect(sf::Vector2f v) {
    setDirection(Util::reflect(getDirection(), v));
}
