#include "../include/Game.h"
#include <SFML/Graphics.hpp>
#include <iostream>
#include <string>
#include "../include/Collision.h"
#include "../include/Brick.h"
#include "../include/Player.h"
#include "../include/Ball.h"
#include "../include/Util.h"
#include "../include/Color.h"
#include <sstream>
#include <vector>

using namespace std;

Game::Game() : player1(0, 0), ball(0, 0) {
    //ctor
}

Game::~Game() {
    //dtor
}

bool Game::initialize() {
    sf::VideoMode videoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32);

    // Create the main rendering window
    window.Create(videoMode, "Brick");
    //window.UseVerticalSync(true);
    //window.SetFramerateLimit(FRAMES_PER_SECOND);

    return true;
}

void Game::createTitle() {
    titleBricks.clear();
    titleBricks.push_back( Brick(BUFFER_LEFT+0*Brick::WIDTH, BUFFER_TOP+0*Brick::HEIGHT, Color::RED) );
    titleBricks.push_back( Brick(BUFFER_LEFT+0*Brick::WIDTH, BUFFER_TOP+1*Brick::HEIGHT, Color::RED) );
    titleBricks.push_back( Brick(BUFFER_LEFT+0*Brick::WIDTH, BUFFER_TOP+2*Brick::HEIGHT, Color::RED) );
    titleBricks.push_back( Brick(BUFFER_LEFT+0*Brick::WIDTH, BUFFER_TOP+3*Brick::HEIGHT, Color::RED) );
    titleBricks.push_back( Brick(BUFFER_LEFT+0*Brick::WIDTH, BUFFER_TOP+4*Brick::HEIGHT, Color::RED) );

    titleBricks.push_back( Brick(BUFFER_LEFT+1*Brick::WIDTH, BUFFER_TOP+0*Brick::HEIGHT, Color::RED) );
    //titleBricks.push_back( Brick(BUFFER_LEFT+1*Brick::WIDTH, BUFFER_TOP+1*Brick::HEIGHT, Color::RED) );
    titleBricks.push_back( Brick(BUFFER_LEFT+1*Brick::WIDTH, BUFFER_TOP+2*Brick::HEIGHT, Color::RED) );
    //titleBricks.push_back( Brick(BUFFER_LEFT+1*Brick::WIDTH, BUFFER_TOP+3*Brick::HEIGHT, Color::RED) );
    titleBricks.push_back( Brick(BUFFER_LEFT+1*Brick::WIDTH, BUFFER_TOP+4*Brick::HEIGHT, Color::RED) );

    titleBricks.push_back( Brick(BUFFER_LEFT+2*Brick::WIDTH, BUFFER_TOP+0*Brick::HEIGHT, Color::RED) );
    titleBricks.push_back( Brick(BUFFER_LEFT+2*Brick::WIDTH, BUFFER_TOP+1*Brick::HEIGHT, Color::RED) );
    titleBricks.push_back( Brick(BUFFER_LEFT+2*Brick::WIDTH, BUFFER_TOP+2*Brick::HEIGHT, Color::RED) );
    titleBricks.push_back( Brick(BUFFER_LEFT+2*Brick::WIDTH, BUFFER_TOP+3*Brick::HEIGHT, Color::RED) );
    titleBricks.push_back( Brick(BUFFER_LEFT+2*Brick::WIDTH, BUFFER_TOP+4*Brick::HEIGHT, Color::RED) );

    //titleBricks.push_back( Brick(BUFFER_LEFT+3*Brick::WIDTH, BUFFER_TOP+0*Brick::HEIGHT, Color::ORANGE) );
    titleBricks.push_back( Brick(BUFFER_LEFT+3*Brick::WIDTH, BUFFER_TOP+1*Brick::HEIGHT, Color::ORANGE) );
    titleBricks.push_back( Brick(BUFFER_LEFT+3*Brick::WIDTH, BUFFER_TOP+2*Brick::HEIGHT, Color::ORANGE) );
    titleBricks.push_back( Brick(BUFFER_LEFT+3*Brick::WIDTH, BUFFER_TOP+3*Brick::HEIGHT, Color::ORANGE) );
    titleBricks.push_back( Brick(BUFFER_LEFT+3*Brick::WIDTH, BUFFER_TOP+4*Brick::HEIGHT, Color::ORANGE) );

    //titleBricks.push_back( Brick(BUFFER_LEFT+4*Brick::WIDTH, BUFFER_TOP+0*Brick::HEIGHT, Color::ORANGE) );
    titleBricks.push_back( Brick(BUFFER_LEFT+4*Brick::WIDTH, BUFFER_TOP+1*Brick::HEIGHT, Color::YELLOW) );
    titleBricks.push_back( Brick(BUFFER_LEFT+4*Brick::WIDTH, BUFFER_TOP+2*Brick::HEIGHT, Color::ORANGE) );
    titleBricks.push_back( Brick(BUFFER_LEFT+4*Brick::WIDTH, BUFFER_TOP+3*Brick::HEIGHT, Color::YELLOW) );
    titleBricks.push_back( Brick(BUFFER_LEFT+4*Brick::WIDTH, BUFFER_TOP+4*Brick::HEIGHT, Color::YELLOW) );

    //titleBricks.push_back( Brick(BUFFER_LEFT+5*Brick::WIDTH, BUFFER_TOP+0*Brick::HEIGHT, Color::GREEN) );
    titleBricks.push_back( Brick(BUFFER_LEFT+5*Brick::WIDTH, BUFFER_TOP+1*Brick::HEIGHT, Color::GREEN) );
    titleBricks.push_back( Brick(BUFFER_LEFT+5*Brick::WIDTH, BUFFER_TOP+2*Brick::HEIGHT, Color::GREEN) );
    titleBricks.push_back( Brick(BUFFER_LEFT+5*Brick::WIDTH, BUFFER_TOP+3*Brick::HEIGHT, Color::GREEN) );
    titleBricks.push_back( Brick(BUFFER_LEFT+5*Brick::WIDTH, BUFFER_TOP+4*Brick::HEIGHT, Color::GREEN) );

    //titleBricks.push_back( Brick(BUFFER_LEFT+6*Brick::WIDTH, BUFFER_TOP+0*Brick::HEIGHT, Color::GREEN) );
    titleBricks.push_back( Brick(BUFFER_LEFT+6*Brick::WIDTH, BUFFER_TOP+1*Brick::HEIGHT, Color::GREEN) );
    //titleBricks.push_back( Brick(BUFFER_LEFT+6*Brick::WIDTH, BUFFER_TOP+2*Brick::HEIGHT, Color::GREEN) );
    //titleBricks.push_back( Brick(BUFFER_LEFT+6*Brick::WIDTH, BUFFER_TOP+3*Brick::HEIGHT, Color::GREEN) );
    titleBricks.push_back( Brick(BUFFER_LEFT+6*Brick::WIDTH, BUFFER_TOP+4*Brick::HEIGHT, Color::GREEN) );

    titleBricks.push_back( Brick(BUFFER_LEFT+7*Brick::WIDTH, BUFFER_TOP+0*Brick::HEIGHT, Color::CYAN) );
    titleBricks.push_back( Brick(BUFFER_LEFT+7*Brick::WIDTH, BUFFER_TOP+1*Brick::HEIGHT, Color::CYAN) );
    titleBricks.push_back( Brick(BUFFER_LEFT+7*Brick::WIDTH, BUFFER_TOP+2*Brick::HEIGHT, Color::CYAN) );
    titleBricks.push_back( Brick(BUFFER_LEFT+7*Brick::WIDTH, BUFFER_TOP+3*Brick::HEIGHT, Color::CYAN) );
    titleBricks.push_back( Brick(BUFFER_LEFT+7*Brick::WIDTH, BUFFER_TOP+4*Brick::HEIGHT, Color::CYAN) );

    //titleBricks.push_back( Brick(BUFFER_LEFT+8*Brick::WIDTH, BUFFER_TOP+0*Brick::HEIGHT, Color::CYAN) );
    //titleBricks.push_back( Brick(BUFFER_LEFT+8*Brick::WIDTH, BUFFER_TOP+1*Brick::HEIGHT, Color::CYAN) );
    titleBricks.push_back( Brick(BUFFER_LEFT+8*Brick::WIDTH, BUFFER_TOP+2*Brick::HEIGHT, Color::CYAN) );
    titleBricks.push_back( Brick(BUFFER_LEFT+8*Brick::WIDTH, BUFFER_TOP+3*Brick::HEIGHT, Color::CYAN) );
    //titleBricks.push_back( Brick(BUFFER_LEFT+8*Brick::WIDTH, BUFFER_TOP+4*Brick::HEIGHT, Color::CYAN) );

    //titleBricks.push_back( Brick(BUFFER_LEFT+9*Brick::WIDTH, BUFFER_TOP+0*Brick::HEIGHT, Color::CYAN) );
    titleBricks.push_back( Brick(BUFFER_LEFT+9*Brick::WIDTH, BUFFER_TOP+1*Brick::HEIGHT, Color::CYAN) );
    //titleBricks.push_back( Brick(BUFFER_LEFT+9*Brick::WIDTH, BUFFER_TOP+2*Brick::HEIGHT, Color::CYAN) );
    //titleBricks.push_back( Brick(BUFFER_LEFT+9*Brick::WIDTH, BUFFER_TOP+3*Brick::HEIGHT, Color::CYAN) );
    titleBricks.push_back( Brick(BUFFER_LEFT+9*Brick::WIDTH, BUFFER_TOP+4*Brick::HEIGHT, Color::CYAN) );
}

void Game::setupGame() {
    // set up the background / screen bounds
    sf::Sprite backgroundSprite(sf::Image(800, 600, Color::BLACK));
    backgroundSprite.SetX(0.f);
    backgroundSprite.SetY(0.f);
    this->backgroundSprite = backgroundSprite;

    // set up the lives text
    sf::String player1LivesText;
    player1LivesText.SetFont(sf::Font::GetDefaultFont());
    player1LivesText.SetSize(32);
    player1LivesText.Move(32, 32);
    this->player1LivesText = player1LivesText;

    // set up the fps text
    sf::String fpsText;
    fpsText.SetFont(sf::Font::GetDefaultFont());
    fpsText.SetSize(32);
    fpsText.Move(SCREEN_WIDTH - 64, SCREEN_HEIGHT - 32);
    this->fpsText = fpsText;

    // set up the game over text
    sf::String gameOverText;
    gameOverText.SetText("Game Over");
    gameOverText.SetFont(sf::Font::GetDefaultFont());
    gameOverText.SetSize(64);
    gameOverText.Move(SCREEN_WIDTH / 2 - gameOverText.GetRect().GetWidth() / 2,
                      SCREEN_HEIGHT / 3 - gameOverText.GetRect().GetHeight() / 2);
    this->gameOverText = gameOverText;

    // set up the game won text
    sf::String gameWonText;
    gameWonText.SetText("Game Won");
    gameWonText.SetFont(sf::Font::GetDefaultFont());
    gameWonText.SetSize(64);
    gameWonText.Move(SCREEN_WIDTH / 2 - gameWonText.GetRect().GetWidth() / 2,
                     SCREEN_HEIGHT / 3 - gameWonText.GetRect().GetHeight() / 2);
    this->gameWonText = gameWonText;

    // set up the game title text
    sf::String titleText;
    titleText.SetText("Press RETURN to begin.");
    titleText.SetFont(sf::Font::GetDefaultFont());
    titleText.SetSize(30);
    titleText.Move(SCREEN_WIDTH / 2 - titleText.GetRect().GetWidth() / 2,
                   SCREEN_HEIGHT / 2 - titleText.GetRect().GetHeight() / 2);
    this->titleText = titleText;

    // set up the restart text
    sf::String restartText;
    restartText.SetText("Press RETURN to play again.");
    restartText.SetFont(sf::Font::GetDefaultFont());
    restartText.SetSize(30);
    restartText.Move(SCREEN_WIDTH / 2 - restartText.GetRect().GetWidth() / 2,
                   3 * SCREEN_HEIGHT / 4 - restartText.GetRect().GetHeight() / 2);
    this->restartText = restartText;

    Player player1(SCREEN_WIDTH / 2 - Player::WIDTH/2, SCREEN_HEIGHT - Player::HEIGHT);
    player1.setVelocity(400);
    this->player1 = player1;

    createTitle();
}

void Game::setupLevel() {

    bricks.clear();

    // Initialize Bricks
    int row = 0;
    for (int y=BUFFER_TOP; y<SCREEN_HEIGHT-BUFFER_BOTTOM-Brick::HEIGHT; y+=Brick::HEIGHT) {

        int column = 0;
        for(int x=BUFFER_LEFT; x<SCREEN_WIDTH-BUFFER_RIGHT-Brick::WIDTH; x+=Brick::WIDTH) {
            sf::Color color;
            if (column % 2 == 1) {
                if (row % 2 == 1) {
                    color = Color::WHITE;
                } else {
                    color = Color::YELLOW;
                }
            } else {
                if (row % 2 == 1) {
                    color = Color::BLUE;
                } else {
                    color = Color::GREEN;
                }
            }

            bricks.push_back( Brick(x, y, color) );
            column++;
        }
        row++;
    }

    setupRound();
}

void Game::setupRound() {
    //put player back in the middle
    player1.setPosition(SCREEN_WIDTH / 2 - Player::WIDTH/2, SCREEN_HEIGHT - Player::HEIGHT);
    player1.setLazers(false);

    // re-initialize ball
    Ball ball(SCREEN_WIDTH / 2 - Ball::WIDTH/2, SCREEN_HEIGHT - Player::HEIGHT - Ball::HEIGHT);
    ball.setVelocity(400);
    this->ball = ball;

    //clear powerups
    powerups.clear();

    //clear lazers
    lazers.clear();
}

int Game::run() {

    setupGame();

    sf::Clock renderClock, updateClock;
    // Start game loop
    while (window.IsOpened()) {

        processEvents();

        switch (gameState) {
        case PLAYING:

            float updateTime = updateClock.GetElapsedTime();
            update(updateTime);
            updateClock.Reset();

            break;
        }

        draw();

//TODO: make this work.  Note: still does not work.
/*
        // time, in seconds
        float time = renderClock.GetElapsedTime();
        float fps = 1 / time;
        renderClock.Reset();

        long frameTime = 1000000 / FRAMES_PER_SECOND;
        sf::Clock c;
        float t = c.GetElapsedTime() * 1000000;
        long nextFrameTime = t + frameTime;
        for(int i=0; t < nextFrameTime && i < MAX_FRAMESKIP; i++) {
            processEvents();
            float updateTime = updateClock.GetElapsedTime();
            update(updateTime);
            updateClock.Reset();
            t = c.GetElapsedTime() * 1000000;
        }
        draw();
*/
    }

    return EXIT_SUCCESS;
}

void Game::processEvents() {
    sf::Event Event;
    while (window.GetEvent(Event)) {
        // Close window : exit
        if (Event.Type == sf::Event::Closed)
            window.Close();
    }

    switch (gameState) {
        case INTRO: {
            if (window.GetInput().IsKeyDown(sf::Key::Return)) {
                setupLevel();
                gameState = PLAYING;
            }
        } break;
        case PLAYING: {
            // initialize player direction
            sf::Vector2f movementVector = Util::createVector(Util::NONE);

            // Move the sprite
            if (window.GetInput().IsKeyDown(sf::Key::Up)
                && Collision::GetAABB(*player1.getSprite()).Top > Collision::GetAABB(backgroundSprite).Top) {
                movementVector = Util::add(movementVector, Util::createVector(Util::UP));
            }

            if (window.GetInput().IsKeyDown(sf::Key::Down)
                && Collision::GetAABB(*player1.getSprite()).Bottom < Collision::GetAABB(backgroundSprite).Bottom) {
                movementVector = Util::add(movementVector, Util::createVector(Util::DOWN));
            }

            if (window.GetInput().IsKeyDown(sf::Key::Left)
                && Collision::GetAABB(*player1.getSprite()).Left > Collision::GetAABB(backgroundSprite).Left) {
                movementVector = Util::add(movementVector, Util::createVector(Util::LEFT));
            }

            if (window.GetInput().IsKeyDown(sf::Key::Right)
                && Collision::GetAABB(*player1.getSprite()).Right < Collision::GetAABB(backgroundSprite).Right) {
                movementVector = Util::add(movementVector, Util::createVector(Util::RIGHT));
            }

            player1.setDirection(Util::unit(movementVector));

            // stick the ball to the player
            if (ball.isLive()) {

                if (player1.hasLazers() && window.GetInput().IsKeyDown(sf::Key::Space) && player1.canFire()) {

                    int x = (*player1.getSprite()).GetPosition().x + Player::WIDTH/2;
                    int y = (*player1.getSprite()).GetPosition().y - Player::HEIGHT/2;

                    Lazer lazer(x, y);
                    lazer.setDirection(Util::createVector(Util::UP));
                    lazer.setVelocity(600);
                    lazers.push_back(lazer);
                }

            } else {
                ball.setDirection(player1.getDirection());

                if (window.GetInput().IsKeyDown(sf::Key::Space)) {
                    ball.setDirection(Util::createVector(Util::UP, Util::RIGHT));
                    ball.setLive(true);
                }
            }
        } break;
        case GAME_LOST: {
            if (window.GetInput().IsKeyDown(sf::Key::Return)) {
                setupGame();
                setupLevel();
                gameState = PLAYING;
            }
        } break;
        case GAME_WON: {
            if (window.GetInput().IsKeyDown(sf::Key::Return)) {
                setupGame();
                setupLevel();
                gameState = PLAYING;
            }
        } break;
    }
}

void Game::checkCollisions() {

    // TODO: destroy powerups and lazers that go off the field

    // close enough for government work
    if (Collision::GetAABB(*ball.getSprite()).Top <= Collision::GetAABB(backgroundSprite).Top
        && ball.getDirection().y < 0) {
        std::cout << "upper screen bounds collision" << std::endl;
        ball.reflect(Util::createVector(Util::DOWN));
    }

    if (Collision::GetAABB(*ball.getSprite()).Bottom >= Collision::GetAABB(backgroundSprite).Bottom
        && ball.getDirection().y > 0) {
        std::cout << "lower screen bounds collision" << std::endl;
        ball.reflect(Util::createVector(Util::UP));

        // decrement lives by 1
        player1.setLives(player1.getLives()-1);

        // re-initialize player and ball position
        setupRound();
    }

    if (Collision::GetAABB(*ball.getSprite()).Left <= Collision::GetAABB(backgroundSprite).Left
        && ball.getDirection().x < 0) {
        std::cout << "left screen bounds collision" << std::endl;
        ball.reflect(Util::createVector(Util::RIGHT));
    }

    if (Collision::GetAABB(*ball.getSprite()).Right >= Collision::GetAABB(backgroundSprite).Right
        && ball.getDirection().x > 0) {
        std::cout << "right screen bounds collision" << std::endl;
        ball.reflect(Util::createVector(Util::LEFT));
    }

    // Player / ball collision detection
    if (Collision::SimpleBoundingBoxTest(*ball.getSprite(), *player1.getSprite())
        && ball.getDirection().y > 0) {
        sf::Vector2f reflectionVector = Collision::collide(*ball.getSprite(), *player1.getSprite());
        ball.reflect(reflectionVector);
        std::cout << "player1 collision" << std::endl;
    }

    // Vector used to sum up the reflection fectors.
    sf::Vector2f reflectionVector;

    // brick collision detection
    for(vector<Brick>::iterator it = bricks.begin(); it < bricks.end(); ++it) {

        bool hit = false;

        // brick / ball
        if (Collision::SimpleBoundingBoxTest(*ball.getSprite(), *(*it).getSprite())) {
            // generate reflection vector
            sf::Vector2f temp = Collision::collide(*ball.getSprite(), *(*it).getSprite());
            reflectionVector = Util::add(reflectionVector, temp);

            hit = true;
        }

        // brick / lazer
        for(vector<Lazer>::iterator iit = lazers.begin(); iit < lazers.end() && !hit; ++iit) {
            if (Collision::SimpleBoundingBoxTest(*(*it).getSprite(), *(*iit).getSprite())) {
                // delete the lazer and assign the new valid iterator
                iit = lazers.erase(iit);

                hit = true;
            }
        }

        if (hit) {
            int x = (*(*it).getSprite()).GetPosition().x;
            int y = (*(*it).getSprite()).GetPosition().y;

            // delete the brick and assign the new valid iterator
            it = bricks.erase(it);

            // randomply spawn a powerup
            if ((rand() % 100) < 10) {
                Powerup powerup(x, y);
                powerup.setDirection(Util::createVector(Util::DOWN));
                powerup.setVelocity(200);
                powerups.push_back(powerup);
            }
        }
    }

    // powerup / player1 collision detection
    for(vector<Powerup>::iterator it = powerups.begin(); it < powerups.end(); ++it) {

        if (Collision::SimpleBoundingBoxTest(*player1.getSprite(), *(*it).getSprite())) {

            int powerupType = (*it).getType();

            // delete the brick and assign the new valid iterator
            it = powerups.erase(it);

            switch (powerupType) {
            case 0://Powerup::Type::LAZER:
                player1.setLazers(true);
                break;
            case 1://Powerup::Type::EXTRA_LIFE:
                player1.setLives(player1.getLives()+1);
                break;
            }
        }
    }

    /*
     * Somewhat sane collision detection.
     * Previously, the reflection vectors were summed up.
     * Now, reflect in the direction that has the
     * greatest magnitude.
     */
    if ( !(reflectionVector.x == 0 && reflectionVector.y == 0) ) {
        if (fabs(reflectionVector.x) > fabs(reflectionVector.y)) {
            reflectionVector.x /= fabs(reflectionVector.x);
            reflectionVector.y = 0;
        } else if (fabs(reflectionVector.y) > fabs(reflectionVector.x)) {
            reflectionVector.y /= fabs(reflectionVector.y);
            reflectionVector.x = 0;
        } else {
            reflectionVector.x /= fabs(reflectionVector.x);
            reflectionVector.y /= fabs(reflectionVector.y);
        }
        ball.reflect(Util::unit(reflectionVector));
    }
}

void Game::update(float elapsedTime) {

    checkCollisions();

    // Update game object positions
    player1.tick(elapsedTime);
    ball.tick(elapsedTime);

    // update powerups
    for(vector<Powerup>::iterator it = powerups.begin(); it != powerups.end(); ++it) {
        (*it).tick(elapsedTime);
    }

    // update lazers
    for(vector<Lazer>::iterator it = lazers.begin(); it != lazers.end(); ++it) {
        (*it).tick(elapsedTime);
    }

    if (player1.getLives() < 0) {
        gameState = GAME_LOST;
    }

    else if (bricks.size() == 0) {
        gameState = GAME_WON;
    }

    player1LivesText.SetText(Util::toString(player1.getLives()));
}

void Game::draw() {

    // Clear screen
    window.Clear();

    switch (gameState) {
    case INTRO:
        // draw title bricks
        for(vector<Brick>::iterator it = titleBricks.begin(); it != titleBricks.end(); ++it) {
            (*it).draw(window);
        }

        window.Draw(titleText);
        break;
    case PLAYING:
        // draw bricks
        for(vector<Brick>::iterator it = bricks.begin(); it != bricks.end(); ++it) {
            (*it).draw(window);
        }

        // draw powerups
        for(vector<Powerup>::iterator it = powerups.begin(); it != powerups.end(); ++it) {
            (*it).draw(window);
        }

        // draw lazers
        for(vector<Lazer>::iterator it = lazers.begin(); it != lazers.end(); ++it) {
            (*it).draw(window);
        }

        player1.draw(window);
        ball.draw(window);

        window.Draw(player1LivesText);
        break;
    case GAME_LOST:
        window.Draw(gameOverText);
        window.Draw(restartText);
        break;
    case GAME_WON:
        window.Draw(gameWonText);
        window.Draw(restartText);
        break;
    }

    // Display window contents on screen
    window.Display();
}

int main() {
    Game game;
    if (!game.initialize()) {
        return EXIT_FAILURE;
    }
    return game.run();
}
